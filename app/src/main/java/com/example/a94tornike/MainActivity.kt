package com.example.a94tornike

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation:String = "0"
    private lateinit var moqmedeba: TextView
    private var pasuxi:Boolean = false
            

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)
        moqmedeba = findViewById(R.id.moqmedeba)

    }
    fun numberClick(clickedView: View){
        if(clickedView is TextView){

            var result: String = resultTextView.text.toString()
            val number: String = clickedView.text.toString()
            if(result == "0")  {
                result = ""
               // resultTextView.text = (result + number).toString()
            }
            else if(result == 0.0.toString()){
                result = ""
                resultTextView.text = (result + number).toString()
            }else{
                resultTextView.text = (result + number).toString()
            }
            if(result == "+"){
                resultTextView.text = number
            }
            else if(result == "-"){
                resultTextView.text = number
            }
            else if(result == "/"){
                resultTextView.text = number
            }
            else if(result == "*"){
                resultTextView.text = number
            }
            else if(result == "%"){
                resultTextView.text = number
            }
            if(pasuxi == true){
                resultTextView.text = ""
                pasuxi = false
            }
        }

    }

    fun operationClick(clickedView: View){

        if(clickedView is TextView){
            val result: String = resultTextView.text.toString()
            if (result.isNotEmpty()){
                this.operand = result.toDouble()
            }
            when (operation) {
                "+" -> resultTextView.text = "+"
                "-" -> resultTextView.text = "-"
                "/" -> resultTextView.text = "/"
                "*" -> resultTextView.text = "*"
                "%" -> resultTextView.text = "%"
                "^2" -> resultTextView.text = "^2"
            }
           // resultTextView.text = ""

            this.operation = clickedView.text.toString()
        }
    }
    fun equalsClick(clickedView: View){
        val result: String = resultTextView.text.toString()
        var secOperand:Double = 0.0

        if(result.isNotEmpty()) {
            secOperand = result.toDouble()
        }else{
            resultTextView.text = "0"
        }
        pasuxi = true
        when (operation) {
            "+" -> {
                if ((operand + secOperand) % 1 == 0.0) {
                    resultTextView.text = (operand + secOperand).toInt().toString()
                } else { resultTextView.text = (operand + secOperand).toString() }
            }
            "-" -> {
                if ((operand - secOperand) % 1 == 0.0) {
                    resultTextView.text = (operand - secOperand).toInt().toString()
                } else { resultTextView.text = (operand - secOperand).toString() }
            }
            "/" -> {
                if ((operand / secOperand) % 1 == 0.0) {
                    resultTextView.text = (operand / secOperand).toInt().toString()
                } else { resultTextView.text = (operand / secOperand).toString() }
            }

            "*" -> {
                if ((operand * secOperand) % 1 == 0.0) {
                    resultTextView.text = (operand * secOperand).toInt().toString()
                } else { resultTextView.text = (operand * secOperand).toString() }
            }

            "%" -> {
                if (((operand / 100) * secOperand) % 1 == 0.0) {
                    resultTextView.text = ((operand / 100) * secOperand).toInt().toString()
                } else { resultTextView.text = ((operand / 100) * secOperand).toString() }
            }
            "^2" -> {
                if ((operand * operand) % 1 == 0.0) {
                resultTextView.text = (operand * operand).toInt().toString()
                }else{resultTextView.text = (operand * operand).toString()}
            }
        }
    }
    fun del(clickedView: View){
        val result: String = resultTextView.text.toString()
        if(clickedView is TextView){
            if(result.isNotEmpty()){
                resultTextView.text = result.dropLast(1)
            }
        }

    }
    fun clear(clickedView: View){

        if(clickedView is TextView){
            resultTextView.text = "0"

        }
    }

}